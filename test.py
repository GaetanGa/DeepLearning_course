import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os
import random
import gc
from sklearn.model_selection import train_test_split
from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import img_to_array, load_img
from keras.callbacks import TensorBoard


# A function to read and process the images to an acceptable format for our model
def read_and_process_image(list_of_images):
    """
    Returns two arrays:
    X is an array of resized images
    y is an array of labels
    """
    X = [] # images
    y = []  # labels

    for image in list_of_images:
        im = cv2.imread(image, cv2.IMREAD_COLOR)
        im2 = cv2.resize(im,(nrows, ncolumns), interpolation=cv2.INTER_CUBIC)
        X.append(im2)
        #  X.append(cv2.resize(cv2.imread(image, cv2.IMREAD_COLOR), (nrows, ncolumns), interpolation=cv2.INTER_CUBIC)) # Read the image

        # get the labels
        if 'dog' in image:
            y.append(1)
        elif 'cat' in image:
            y.append(0)

    return X, y

#__Directory
train_dir = 'data/train'
test_dir = 'data/test'

#__Image dimensions
nrows = 150
ncolumns = 150
channels = 3

#__Get dogs images
train_dogs = ['data/train/{}'.format(i) for i in os.listdir(train_dir) if 'dog' in i]

#__Get cats images
train_cats = ['data/train/{}'.format(i) for i in os.listdir(train_dir) if 'cat' in i]

#__Get tests images
test_imgs = ['data/validation/{}'.format(i) for i in os.listdir(test_dir)]

#__Get only 2000 images each
train_imgs = train_dogs[:2000] + train_cats[:2000]

random.shuffle(train_imgs)

#Launch the function who process the images
X, y = read_and_process_image(train_imgs)
X = np.array(X)
y = np.array(y)

#__Splitting data (train / test)
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.20, random_state=2)
print("Shape of train images is:", X_train.shape)
print("Shape of validation images is:", X_val.shape)
print("Shape of labels is:", y_train.shape)
print("Shape of labels is:", y_val.shape)

#__Clear memory
del X
del y
gc.collect()

#__Get the length of the train and validation data
ntrain = len(X_train)
nval = len(X_val)

batch_size = 32

model = models.Sequential()
model.add(layers.conv2D(32, (3, 3), activation='relu', input_shape=(150,150,3)))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Conv2D(64, (3,3), activation='relu'))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Flatten())
model.add(layers.Dropout(0.5))
model.add(layers.Dense(512, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))
model.summary()
model.compile(loss='binary_crossentropy', optimizer=optimizers.RMSprop(lr=1e-4), metrics=['acc'])

train_datagen = ImageDataGenerator(rescale=1./255,
                                   rotation_range=40,
                                   width_shift_range=0.2,
                                   height_shift_range=0.2,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True,)

val_datagen = ImageDataGenerator(rescale=1./255)

#_Create the image generators
train_generator = train_datagen.flow(X_train, y_train, batch_size=batch_size)
val_generator = val_datagen.flow(X_val, y_val, batch_size=batch_size)
history = model.fit_generator(train_generator,
                              steps_per_epoch=ntrain // batch_size,
                              epochs=32,
                              validation_data=val_generator,
                              validation_steps=nval // batch_size)

NAME = "CNN_FramScratch"

history = model.fit_generator(train_generator,
                              steps_per_epoch=ntrain // batch_size,
                              epochs=5,
                              validation_data=val_generator,
                              validation_steps=nval // batch_size,
                              callbacks=[TensorBoard])
